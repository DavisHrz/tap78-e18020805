package practicas;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author David
 */
public class Listener3 extends JFrame {
    
    JButton btn1;
    
    public Listener3(){
        this.setLayout(new BorderLayout());
        
        btn1 = new JButton("Salir");
        
        btn1.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
            
        });
        
        this.add(btn1, BorderLayout.CENTER);
    }
    
    public static void main(String args[]){
        Listener3 ventana = new Listener3();
        ventana.setSize(300, 200);
        ventana.setVisible(true);
    }
    
}