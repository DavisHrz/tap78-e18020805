package practicas.practica1;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author David Hrz
 */

public class Practica1 extends JFrame {
    
    JLabel lb1;
    JTextField tf;
    JButton btn;
    
    public Practica1(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());
        this.setSize(400, 150);
        lb1 = new JLabel("Escribe un nombre para saludar");
        tf = new JTextField();
        btn = new JButton("Saludar");
        
        btn.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                JOptionPane.showMessageDialog(null, "Hola "+tf.getText()+"!!!");
            }
        });
        
        this.add(lb1, BorderLayout.PAGE_START);
        this.add(tf, BorderLayout.CENTER);
        this.add(btn, BorderLayout.PAGE_END);
    }
    
    public static void main(String arg[]){
        Practica1 p1 = new Practica1();
        p1.setVisible(true);
    }
    
}
