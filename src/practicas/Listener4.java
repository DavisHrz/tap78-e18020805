package practicas;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * @author David
 */
public class Listener4 extends JFrame {
    
  JButton btn1;
  
    public Listener4(){
        this.setLayout(new BorderLayout());
        
        btn1 = new JButton("Salir");
        
        btn1.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
                System.exit(0);
            }
        });

        this.add(btn1, BorderLayout.CENTER);
    }
    
    public static void main(String args[]){
        Listener4 ventana = new Listener4();
        ventana.setSize(300, 200);
        ventana.setVisible(true);
    }  
    
}
