package practicas;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * @author David
 */
public class Listener2 extends JFrame implements ActionListener {
    
    JButton btn1, btn2;
    
    public Listener2(){
        this.setLayout(new BorderLayout());
        
        btn1 = new JButton("Abrir");
        btn2 = new JButton("Cerrar");
        
        MiListener ml = new MiListener();
        btn1.addActionListener(ml);
        btn2.addActionListener(ml);
        
        this.add(btn1,BorderLayout.PAGE_START);
        this.add(btn2,BorderLayout.PAGE_END);
    }
    
    public static void main(String args[]){
        Listener2 ventana = new Listener2();
        ventana.setSize(300, 200);
        ventana.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent evento) {
        
        String cmd = evento.getActionCommand();
        if (cmd.equals("Abrir")){
            System.out.println("Se presiono el boton Abrir");
        } else {
            System.out.println("Se presiono el boton Cerrar");
        }
         
    }   
}