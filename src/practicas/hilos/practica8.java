/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicas.hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author macpro1
 */
public class practica8 {
    
    public static void main(String args[]){
        
        (new Thread(new TareaX("Tarea1",10))).start();
        
        (new Thread(new TareaX("Tarea2",5))).start();
        
        (new Thread(new TareaX("Tarea3",20))).start();
        
    }
}

class TareaX implements Runnable {

    int i=0;
    String nombre;
    
    public TareaX(String nombre,int i){
        this.i = i;
        this.nombre = nombre;
    }
    
    @Override
    public void run() {
        for (int x = 0; x < this.i; x++){
            System.out.println(this.nombre + "- Hola mundo "+x);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(TareaX.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}