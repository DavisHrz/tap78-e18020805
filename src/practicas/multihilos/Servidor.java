/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicas.multihilos;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DavidHrz
 */

public class Servidor {
    
    ServerSocket ss;
    
    InputStreamReader in;
    PrintWriter out;
    
    String [][] usuarios = {{"gmendez","123"},
                            {"bfernandez","321"},
                            {"ktamariz","456"},
                            {"llopez","654"},
                            {"kjordan","789"}}; 
    
    ArrayList<Conexion> conexiones;
    
    public static void main(String args[]) {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                (new Servidor()).run();
            }
        });
    }

    private void run() {
        conexiones = new ArrayList<Conexion>();
        
        try {
            ss = new ServerSocket(4444);
            
            System.out.println("Server en espera de conexiones");
            
            while (true){
                Socket socket = ss.accept();
                Conexion cnx = new Conexion(this, socket, this.conexiones.size());
                
                this.conexiones.add(cnx);
                
                cnx.start();
            }
   
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void difundir(String id, String mensaje){
        for (int i = 0; i < this.conexiones.size(); i++) {
            Conexion cnx = this.conexiones.get(i);
            if( cnx.sc.isConnected() && !id.equals(cnx.id)){
                cnx.enviar(id, mensaje);
            }
        }
    }
    
    class Conexion extends Thread {
        
        final int PEDIR_USER  = 0;
        final int PEDIR_PASS  = 1;
        final int AUTENTICADO = 2;
        final int CHAT = 3;
        
        int estado = PEDIR_USER;
        
        Socket sc;
        PrintWriter out;
        BufferedReader in;
        
        Servidor padre;
        int num;
        String id;
        
        public Conexion(Servidor padre, Socket socket, int num){
            this.sc = socket;
            this.padre = padre;
            this.num = num;
            this.id = this.sc.getInetAddress().getHostAddress()+this.num;
        }

        @Override
        public void run() {
            String ipOrg ="";
            int numUsr = -1;
            try {
                
                ipOrg = sc.getInetAddress().getHostAddress();
                
                System.out.println("Recibimos la conexion del cliente:");
                System.out.println(ipOrg);
                
                out = new PrintWriter(sc.getOutputStream(), true);                   
                in = new BufferedReader( new InputStreamReader(sc.getInputStream()));
                
                String usuario = "";
                String pass = "";
                String mensaje = "";

                while(!mensaje.toLowerCase().equals("salir")){
                    
                    switch (this.estado){
                        
                        case PEDIR_USER:
                            out.println("Bienvenido. Proporcione su usuario:");
                            usuario = in.readLine();
                            
                            System.out.printf(ipOrg+"Intentando ingresar el usuario %s\n",usuario);
                            
                            boolean found = false;
                            for (int i=0 ; i<usuarios.length; i++){
                                if(usuario.equals(usuarios[i][0])){
                                    found = true;
                                    numUsr = i;
                                }  
                            }
                            
                            if (found){
                                this.estado = PEDIR_PASS;
                                System.out.printf("usuario %s\n existente",usuario);
                            } else { 
                                System.out.printf("usuario %s\n no existe, se solicita nuevamente",usuario); 
                            }
                            
                            break;
                        case PEDIR_PASS:
                            out.println("Proporcione el password:");
                            pass = in.readLine();
                            
                            if (usuarios[numUsr][1].equals(pass)){
                                this.estado = AUTENTICADO;
                                System.out.println(ipOrg+" Password correcto");
                            } else {
                                System.out.printf("Password incorrecto para usuario %s\n",usuario);
                            }
                            break;
             
                        case AUTENTICADO:
                            this.estado = CHAT;
                            out.println("Usuario autenticado");
                            System.out.printf(ipOrg+"Usuario %s, autenticado correctamente\n",usuario);
                            break;
                            
                        case CHAT:
                            mensaje = in.readLine();
                            System.out.println(ipOrg+": "+mensaje);
                            
                            this.padre.difundir(this.id, mensaje);
                    }
                    
                }
                
                sc.close();
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }                
            
        }

        private void enviar(String id, String mensaje) {
            out.println(id+" - "+mensaje);
        }
    }
    
}
