/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicas.multihilos;

import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;

/**
 *
 * @author DavidHrz
 */

public class Cliente {
    Socket sc;
    InputStreamReader in;
    PrintWriter out;
    Scanner consola;
    
    public static void main(String args[]) {
 
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                (new Cliente()).run();
            }
        });
    }

    private void run() {
        String comando = "", respuesta = "";
        try {
            //25.60.240.78
            sc = new Socket("localhost",4444);
            
            consola = new Scanner(System.in);
            
            PrintWriter out =
                new PrintWriter(sc.getOutputStream(), true);                   
            BufferedReader in = new BufferedReader(
                new InputStreamReader(sc.getInputStream()));

            while (!comando.toLowerCase().contains("salir")){
                respuesta = in.readLine();
                System.out.println(respuesta);
                comando = consola.nextLine();
                out.println(comando);
            }
            
            sc.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
